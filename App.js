import React, { Component } from 'react';
import { StatusBar, AppRegistry } from 'react-native'
import SplashScreen from 'react-native-splash-screen'
import LoggedOut from './src/screens/LoggedOut'
import AppNavigator from './src/navigators/AppNavigator'
import localstore from './localstore' 
import { observer } from 'mobx-react';
import {getByUserInfo } from './userAsyncStorage'


StatusBar.setBarStyle('light-content', true);

@observer
class App extends Component {

  componentDidMount() {
      getByUserInfo.then(function(cevap){
        SplashScreen.hide();  
      }).catch(function(hata){
        SplashScreen.hide(); 
      })
  }

  render() {
   return ( localstore.userId===-1 ? <LoggedOut /> : <AppNavigator /> )
  }
}

AppRegistry.registerComponent('App', () => App);

export default App;