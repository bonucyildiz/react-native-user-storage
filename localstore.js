import { observable , action } from 'mobx';
import { setUserInfo,getByUserInfo } from './/userAsyncStorage';

class localstore {

    @observable userId=null;
    @observable displayName=null;
    @observable activation=null;
    @observable readStatus=false;

    constructor()
    {
        getByUserInfo();
    }

    @action setuserId (id)
    {this.userId=id;}

    @action setdisplayName(name)
    {this.displayName=name;}

    @action setactivation(status)
    {this.activation=status;}

    @action createUser(userId,displayName,activation)
    {
        setUserInfo(userId,displayName,activation);
    }

    @action setReadStatus(status)
    {this.readStatus=status;}



}
export default new localstore();